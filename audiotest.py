#!/usr/bin/python3

from playsound import playsound
import threading
from datetime import datetime
from time import sleep
import random

def play(name,path='./english/',extension='.mp3'):
	if music.is_alive():
		fullpath = f'{path}{name}{extension}'
		print(fullpath) 
		playsound(fullpath)

def bgMusic(once=False):
	if once:
		play(f'sound{weathernum}') 
	else:
		play(f'm{weathernum}')

def greet():
	if hour < 12:
		greeting = 'goodmorning'
	else:
		greeting = 'hello'
	play(f'greet_{greeting}')
	sleep(1)
	play('its')
	play(f'hour_{realhour}')
	play(f'min_{minute}')
	if greeting == 'goodmorning':
		play('ampm_am')
	else:
		play('ampm_pm')
	play(f'week_{weekday}')
	play(f"music_{weather}")


def run(once=False):
	print('start')
	music.start()
	sleep(3)
	if once:
		greet()
	else:
		while music.is_alive():
			greet()
			sleep(6.4)
	print('end')

if __name__ == '__main__':
	path = './english/'
	now = datetime.now()
	minute = now.minute
	if str(minute) == '0':
		minute = '00'
	hour = now.hour
	if hour > 12:
		realhour = hour-12
	else:
		realhour = hour
	weathernum = random.randint(1,7)
	
	weather = ['sunny','partrycloudy','cloudy','foggy','stormy','rainy','snowy']
	weather = weather[weathernum-1]

	day = datetime.today().weekday()
	weekday = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'][day]
	print(f'[{weather} {weathernum}] {weekday} {day} - {hour}:{minute}')



	once=True


	music = threading.Thread(target=bgMusic, args=(once,), daemon=False)


	run(once)
